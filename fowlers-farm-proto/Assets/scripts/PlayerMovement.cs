﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public int force;
	public int runningExtraForce;
	public float rot;

	public Rigidbody2D body;

	void updateRotToMousePosition() {
		Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
		diff.Normalize();

		rot = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler(0f, 0f, rot - 90);
	}

	void move(float x, float y) {
		body.AddForce(new Vector2(x,y));
	}

	void moveCamera() {
		Camera.main.transform.position = new Vector3(
			this.transform.position.x,
			this.transform.position.y,
			this.transform.position.z - 9
		);
	}

	void movePlayer() {
		float extraForce = 0;

		extraForce = runningExtraForce;

		extraForce = 0;

		if(Input.GetKey("w")) { // Up
			move(0,force + extraForce);
		}
		if(Input.GetKey("s")) { // Down
			move(0,-(force + extraForce));
		}
		if(Input.GetKey("d")) { // Right
			move(force + extraForce,0);
		}
		if(Input.GetKey("a")) { // Left
			move(-(force + extraForce),0);
		}
	}

	void Start() {
		body = this.GetComponent<Rigidbody2D>();
	}

	void Update () {
		updateRotToMousePosition();
		moveCamera();
		movePlayer();
	}
}
